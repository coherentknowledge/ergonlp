% description of phrast information fields in ErgoNLP
% copyright Coherent Knowledge 2014-2017
% open source intellectual property, part of documentation for ErgoNLP
%  which has an Apache License version 2.0

version date:
- v1 July 7, 2017

o Overall, ErgoNLP takes as input one or more English sentences and outputs
a set of logical sentences in (the Ergo Lite subset of) Ergo that represent
the parse++ information produced by the Stanford CoreNLP natural language
interpretation annotators.  

("parse++" here means an extension of the parse that includes coreference and
named entity recognition information.)

o ph stands for PHrast, which is in turn short for phrasal term,
i.e., a logical term that designates a phrase.

Since the parse++ output of ErgoNLP is a dependency parse,
each node in the parse tree represents both
1. a subtree -- itself a phrase; and 
2. word (or other similar NL token such as # 's " or "$") and

A term such as ph(104) represents a node in the parse tree,
thus represents both a word and the corresponding subtree of the parse tree.
Here, the "104" is called a phrast numerical id.  

The numbering scheme for phrast numerical id's is
- 1xx for phrasts that result from the first sentence in the input,
  starting with 101
- 2xx for phrasts that result from the second sentence in the input,
  starting with 201
- 3xx for phrasts that result from the third sentence in the input,
  starting for 301

RESTRICTION/ASSUMPTION:  
In principle, sentences might have more than 99 words.
But ErgoNLP is not intended to work on such sentences,
since its goal is to support deep NL understanding that supports complex
logical query answering.  

o root indicates whether a phrast is the root of an input NL sentence.

E.g., the fact ph(107)[root->\true] represents that the phrast node ph(107)
is the root of the/a input sentence.  

o ws stands for word sense.  The word sense includes several different
aspects of the word:  
- wd stands for the WorD token itself.
  E.g., ph(107)[ws(wd)->'days'] represents that
  the phrast ph(107) has word "days".
- PoS stands for Part of Speech.
  E.g., ph(107)[ws(PoS)->'NNS' represents that the PoS for that phrast is NNS
  (singular noun).
  The PoS labels (here, NNS) are from the SCN parser, taken mainly from the
  usual Penn TreeBank set of such PoS labels.  
- ne stands for Named Entity, cf. named entity recognition.
  E.g., ph(107)[ws(ne)->DURATION] represents that the named entity for that
  phrast is DURATION.
- coref stands for COREFerence, cf. coreference analysis.
  E.g., ph(110)[ws(wd)->ph(102)] represents that phrast ph(110) is a
  coreference to the phrast ph(102).  

o ldp stands for Left DePendency.
  rdp stands for Right DePendency.
  dp stands for DePendency edge tuple.      

E.g., ph(107)[ldp(1)->dp(nsubj,ph(104)] represents that the phrast ph(107)
has a dependency edge labeled nsubj (subject noun phrase) to the phrast ph(104).

The dependency arc labels are from the SCN parser, taken mainly from the usual
Penn TreeBank set of such dependency edge labels. 

Each ldp has an associated positive integer number that represents
its left-to-right sequential position within the list of left dependencies.
E.g., ldp(1) is the leftmost within that list, ldp(2) is the next leftmost,
and so on.

Each rdp likewise has an associated positive integer number that represents
its left-to-right sequential position within the list of right dependencies.
E.g., rdp(1) is the leftmost, rdp(2) is the next leftmost, and so on.

%%%% Quickie Review of relevant Ergo syntax for basic frame facts:

subject[property->value] is a triple fact in frame syntax.
  It intuitively corresponds in predicate calculus syntax to
  a triple fact of the form:  property(subject,value).  

subj[prop1->val1,  prop2->val2, prop3->val3]
  is logically equivalent to the following 3 triple facts:
subj[prop1->val1] \and
subj[prop2->val2] \and
subj[prop3->val3].  


%%%% Example 1 of ErgoNLP output -- starting with input sentence as a comment.

// Sentence: The firm's payables is 30 days and the firm's average sales are $50,000.

ph(107)[ ws(wd)->'days',
  root -> \true, 
  ws(PoS)->'NNS',
  ws(ne)->DURATION,
  ldp(1)->dp(nsubj,ph(104)),
  ldp(2)->dp(cop,ph(105)),
  ldp(3)->dp(num,ph(106)),
  rdp(1)->dp(cc,ph(108)),
  rdp(2)->dp(conj,ph(115))].

ph(104)[ ws(wd)->'payables',
  ws(PoS)->'NNS',
  ldp(1)->dp(poss,ph(102))].

ph(105)[ ws(wd)->'is',
  ws(PoS)->'VBZ'].

ph(106)[ ws(wd)->'30',
  ws(PoS)->'CD',
  ws(ne)->DURATION].

ph(108)[ ws(wd)->'and',
  ws(PoS)->'CC'].

ph(115)[ ws(wd)->'$',
  ws(PoS)->'$',
  ws(ne)->MONEY,
  ldp(1)->dp(nsubj,ph(113)),
  ldp(2)->dp(cop,ph(114)),
  rdp(1)->dp(num,ph(116))].

ph(102)[ ws(wd)->'firm',
  ws(PoS)->'NN',
  ldp(1)->dp(det,ph(101)),
  rdp(1)->dp(possessive,ph(103))].

ph(113)[ ws(wd)->'sales',
  ws(PoS)->'NNS',
  ldp(1)->dp(poss,ph(110)),
  ldp(2)->dp(amod,ph(112))].

ph(114)[ ws(wd)->'are',
  ws(PoS)->'VBP'].

ph(116)[ ws(wd)->'50,000',
  ws(PoS)->'CD',
  ws(ne)->MONEY].

ph(101)[ ws(wd)->'The',
  ws(PoS)->'DT'].

ph(103)[ ws(wd)->''s',
  ws(PoS)->'POS'].

ph(110)[ ws(wd)->'firm',
  ws(PoS)->'NN',
  ldp(1)->dp(det,ph(109)),
  rdp(1)->dp(possessive,ph(111)),
  ws(coref)->ph(102)].

ph(112)[ ws(wd)->'average',
  ws(PoS)->'JJ'].

ph(109)[ ws(wd)->'the',
  ws(PoS)->'DT'].

ph(111)[ ws(wd)->''s',
  ws(PoS)->'POS'].

%%%%

Example 2 of ErgoNLP output.

Input passage of 4 sentences:

Eagle Sporting Goods has $2.5 million in inventory and $2 million in accounts receivable.  Its average daily sales are $100,000.  The firm's payables deferral period is 30 days and average daily cost of sales are $50,000.  What is the length of the firm's cash conversion period?

%%

// Sentence: The firm's payables is 30 days and the firm's average sales are $50,000.
ph(107)[ ws(wd)->'days',
  root -> \true, 
  ws(PoS)->'NNS',
  ws(ne)->DURATION,
  ldp(1)->dp(nsubj,ph(104)),
  ldp(2)->dp(cop,ph(105)),
  ldp(3)->dp(num,ph(106)),
  rdp(1)->dp(cc,ph(108)),
  rdp(2)->dp(conj,ph(115))].

ph(104)[ ws(wd)->'payables',
  ws(PoS)->'NNS',
  ldp(1)->dp(poss,ph(102))].

ph(105)[ ws(wd)->'is',
  ws(PoS)->'VBZ'].

ph(106)[ ws(wd)->'30',
  ws(PoS)->'CD',
  ws(ne)->DURATION].

ph(108)[ ws(wd)->'and',
  ws(PoS)->'CC'].

ph(115)[ ws(wd)->'$',
  ws(PoS)->'$',
  ws(ne)->MONEY,
  ldp(1)->dp(nsubj,ph(113)),
  ldp(2)->dp(cop,ph(114)),
  rdp(1)->dp(num,ph(116))].

ph(102)[ ws(wd)->'firm',
  ws(PoS)->'NN',
  ldp(1)->dp(det,ph(101)),
  rdp(1)->dp(possessive,ph(103))].

ph(113)[ ws(wd)->'sales',
  ws(PoS)->'NNS',
  ldp(1)->dp(poss,ph(110)),
  ldp(2)->dp(amod,ph(112))].

ph(114)[ ws(wd)->'are',
  ws(PoS)->'VBP'].

ph(116)[ ws(wd)->'50,000',
  ws(PoS)->'CD',
  ws(ne)->MONEY].

ph(101)[ ws(wd)->'The',
  ws(PoS)->'DT'].

ph(103)[ ws(wd)->''s',
  ws(PoS)->'POS'].

ph(110)[ ws(wd)->'firm',
  ws(PoS)->'NN',
  ldp(1)->dp(det,ph(109)),
  rdp(1)->dp(possessive,ph(111)),
  ws(coref)->ph(102)].

ph(112)[ ws(wd)->'average',
  ws(PoS)->'JJ'].

ph(109)[ ws(wd)->'the',
  ws(PoS)->'DT'].

ph(111)[ ws(wd)->''s',
  ws(PoS)->'POS'].

%%%%

// Sentence: Eagle Sporting Goods has $2.5 million in inventory and $2 million in accounts receivable.
ph(104)[ ws(wd)->'has',
  root -> \true, 
  ws(PoS)->'VBZ',
  ldp(1)->dp(nsubj,ph(103)),
  rdp(1)->dp(dobj,ph(105)),
  rdp(2)->dp(cc,ph(110)),
  rdp(3)->dp(conj,ph(116))].

ph(103)[ ws(wd)->'Goods',
  ws(PoS)->'NNP',
  ws(ne)->ORGANIZATION,
  ldp(1)->dp(nn,ph(101)),
  ldp(2)->dp(nn,ph(102))].

ph(105)[ ws(wd)->'$',
  ws(PoS)->'$',
  ws(ne)->MONEY,
  rdp(1)->dp(num,ph(107)),
  rdp(2)->dp(prep,ph(108))].

ph(110)[ ws(wd)->'and',
  ws(PoS)->'CC'].

ph(116)[ ws(wd)->'receivable',
  ws(PoS)->'JJ',
  ldp(1)->dp(dep,ph(111))].

ph(101)[ ws(wd)->'Eagle',
  ws(PoS)->'NNP',
  ws(ne)->ORGANIZATION].

ph(102)[ ws(wd)->'Sporting',
  ws(PoS)->'NNP',
  ws(ne)->ORGANIZATION].

ph(107)[ ws(wd)->'million',
  ws(PoS)->'CD',
  ws(ne)->MONEY,
  ldp(1)->dp(number,ph(106))].

ph(108)[ ws(wd)->'in',
  ws(PoS)->'IN',
  rdp(1)->dp(pobj,ph(109))].

ph(111)[ ws(wd)->'$',
  ws(PoS)->'$',
  ws(ne)->MONEY,
  rdp(1)->dp(num,ph(113)),
  rdp(2)->dp(prep,ph(114))].

ph(106)[ ws(wd)->'2.5',
  ws(PoS)->'CD',
  ws(ne)->MONEY].

ph(109)[ ws(wd)->'inventory',
  ws(PoS)->'NN'].

ph(113)[ ws(wd)->'million',
  ws(PoS)->'CD',
  ws(ne)->MONEY,
  ldp(1)->dp(number,ph(112))].

ph(114)[ ws(wd)->'in',
  ws(PoS)->'IN',
  rdp(1)->dp(pobj,ph(115))].

ph(112)[ ws(wd)->'2',
  ws(PoS)->'CD',
  ws(ne)->MONEY].

ph(115)[ ws(wd)->'accounts',
  ws(PoS)->'NNS'].

// Sentence: Its average daily sales are $100,000.
ph(206)[ ws(wd)->'$',
  root -> \true, 
  ws(PoS)->'$',
  ws(ne)->MONEY,
  ldp(1)->dp(nsubj,ph(204)),
  ldp(2)->dp(cop,ph(205)),
  rdp(1)->dp(num,ph(207))].

ph(204)[ ws(wd)->'sales',
  ws(PoS)->'NNS',
  ldp(1)->dp(poss,ph(201)),
  ldp(2)->dp(amod,ph(202)),
  ldp(3)->dp(amod,ph(203))].

ph(205)[ ws(wd)->'are',
  ws(PoS)->'VBP'].

ph(207)[ ws(wd)->'100,000',
  ws(PoS)->'CD',
  ws(ne)->MONEY].

ph(201)[ ws(wd)->'Its',
  ws(PoS)->'PRP$',
  ws(coref)->ph(103)].

ph(202)[ ws(wd)->'average',
  ws(PoS)->'JJ'].

ph(203)[ ws(wd)->'daily',
  ws(PoS)->'JJ',
  ws(ne)->SET].

// Sentence: The firm's payables deferral period is 30 days and average daily cost of sales are $50,000.
ph(309)[ ws(wd)->'days',
  root -> \true, 
  ws(PoS)->'NNS',
  ws(ne)->DURATION,
  ldp(1)->dp(nsubj,ph(304)),
  ldp(2)->dp(tmod,ph(306)),
  ldp(3)->dp(cop,ph(307)),
  ldp(4)->dp(num,ph(308)),
  rdp(1)->dp(cc,ph(310)),
  rdp(2)->dp(conj,ph(317))].

ph(304)[ ws(wd)->'payables',
  ws(PoS)->'NNS',
  ldp(1)->dp(poss,ph(302))].

ph(306)[ ws(wd)->'period',
  ws(PoS)->'NN',
  ldp(1)->dp(amod,ph(305))].

ph(307)[ ws(wd)->'is',
  ws(PoS)->'VBZ'].

ph(308)[ ws(wd)->'30',
  ws(PoS)->'CD',
  ws(ne)->DURATION].

ph(310)[ ws(wd)->'and',
  ws(PoS)->'CC'].

ph(317)[ ws(wd)->'$',
  ws(PoS)->'$',
  ws(ne)->MONEY,
  ldp(1)->dp(nsubj,ph(313)),
  ldp(2)->dp(cop,ph(316)),
  rdp(1)->dp(num,ph(318))].

ph(302)[ ws(wd)->'firm',
  ws(PoS)->'NN',
  ldp(1)->dp(det,ph(301)),
  rdp(1)->dp(possessive,ph(303))].

ph(305)[ ws(wd)->'deferral',
  ws(PoS)->'JJ'].

ph(313)[ ws(wd)->'cost',
  ws(PoS)->'NN',
  ldp(1)->dp(amod,ph(311)),
  ldp(2)->dp(amod,ph(312)),
  rdp(1)->dp(prep,ph(314))].

ph(316)[ ws(wd)->'are',
  ws(PoS)->'VBP'].

ph(318)[ ws(wd)->'50,000',
  ws(PoS)->'CD',
  ws(ne)->MONEY].

ph(301)[ ws(wd)->'The',
  ws(PoS)->'DT'].

ph(303)[ ws(wd)->''s',
  ws(PoS)->'POS'].

ph(311)[ ws(wd)->'average',
  ws(PoS)->'JJ'].

ph(312)[ ws(wd)->'daily',
  ws(PoS)->'JJ',
  ws(ne)->SET].

ph(314)[ ws(wd)->'of',
  ws(PoS)->'IN',
  rdp(1)->dp(pobj,ph(315))].

ph(315)[ ws(wd)->'sales',
  ws(PoS)->'NNS',
  ws(coref)->ph(204)].

// Sentence: What is the length of the firm's cash conversion period?
ph(401)[ ws(wd)->'What',
  root -> \true, 
  sentenceType -> question, 
  ws(PoS)->'WP',
  rdp(1)->dp(cop,ph(402)),
  rdp(2)->dp(nsubj,ph(404))].

ph(402)[ ws(wd)->'is',
  ws(PoS)->'VBZ'].

ph(404)[ ws(wd)->'length',
  ws(PoS)->'NN',
  ldp(1)->dp(det,ph(403)),
  rdp(1)->dp(prep,ph(405))].

ph(403)[ ws(wd)->'the',
  ws(PoS)->'DT'].

ph(405)[ ws(wd)->'of',
  ws(PoS)->'IN',
  rdp(1)->dp(pobj,ph(411))].

ph(411)[ ws(wd)->'period',
  ws(PoS)->'NN',
  ldp(1)->dp(poss,ph(407)),
  ldp(2)->dp(nn,ph(409)),
  ldp(3)->dp(nn,ph(410))].

ph(407)[ ws(wd)->'firm',
  ws(PoS)->'NN',
  ldp(1)->dp(det,ph(406)),
  rdp(1)->dp(possessive,ph(408)),
  ws(coref)->ph(302)].

ph(409)[ ws(wd)->'cash',
  ws(PoS)->'NN'].

ph(410)[ ws(wd)->'conversion',
  ws(PoS)->'NN'].

ph(406)[ ws(wd)->'the',
  ws(PoS)->'DT'].

ph(408)[ ws(wd)->''s',
  ws(PoS)->'POS'].

%%%%

