/* File:      ErgoNLPGUI.java
**
** Author(s): Paul Fodor
**
** Contact:   info@coherentknowledge.com
** 
** Copyright (C) Coherent Knowledge Systems, 2015-2017
** 
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**      http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
** 
*/

package com.coherentknowledge.ergonlp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.text.DefaultCaret;

public class ErgoNLPGUI extends JFrame implements ActionListener {
	static JTextArea inputSentenceTextArea;
	static JTextArea outputTextArea;
	static JButton parseButton;

	public ErgoNLPGUI() {
		// input area:
		JPanel inputPanel = new JPanel();
		inputPanel.setBorder(new LineBorder(Color.BLACK, 1));
		inputPanel.setLayout(new BorderLayout());
		inputPanel.setBackground(SystemColor.control);
		inputPanel.setForeground(Color.RED);
		this.add(inputPanel, BorderLayout.NORTH);
		JLabel enterSentencelabel = new JLabel("Enter the sentences: ");
		inputPanel.add(enterSentencelabel, BorderLayout.WEST);
		inputSentenceTextArea = new JTextArea(3, 40);
		inputSentenceTextArea.setBorder(new LineBorder(Color.BLACK, 1));
		inputSentenceTextArea.setLineWrap(true);
		inputSentenceTextArea.setWrapStyleWord(true);
		inputPanel.add(inputSentenceTextArea, BorderLayout.CENTER);
		parseButton = new JButton("Parse");
		parseButton.addActionListener(this);
		inputPanel.add(parseButton, BorderLayout.EAST);
		// output area:
		JPanel outputPanel = new JPanel();
		outputPanel.setLayout(new BorderLayout());
		outputPanel.setBackground(SystemColor.control);
		outputPanel.setForeground(Color.RED);
		this.add(outputPanel, BorderLayout.SOUTH);
		JLabel outputLabel = new JLabel("Output: ");
		outputPanel.add(outputLabel, BorderLayout.NORTH);
		JPanel southOutput = new JPanel(new GridLayout(2, 1));
		JLabel versionLabel = new JLabel("ErgoNLP version 0.3.3 Date: June 22, 2017.");
		JLabel copyrightLabel = new JLabel("� Copyright 2017 CoherentKnowledge Systems");
		southOutput.add(versionLabel);
		southOutput.add(copyrightLabel);
		outputPanel.add(southOutput, BorderLayout.SOUTH);
		outputTextArea = new JTextArea(40, 40);
		DefaultCaret caret = (DefaultCaret) outputTextArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		JScrollPane scrollPane = new JScrollPane(outputTextArea);
		outputPanel.add(scrollPane);
	}

	public static void main(String[] args) {
		ErgoNLPGUI myswing = new ErgoNLPGUI();
		myswing.setTitle("ErgoNLP");
		myswing.setSize(800, 800);
		myswing.setDefaultCloseOperation(EXIT_ON_CLOSE);
		myswing.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == parseButton) {
			SCN2Ergo scn2Flora2Translate = new SCN2Ergo();
			String sentence_t = inputSentenceTextArea.getText();
			// System.out.println("% " + sentence_t);
			scn2Flora2Translate.parseSentence(sentence_t);
			String string = scn2Flora2Translate.print2String();
			outputTextArea.append(string);
			// System.out.println("Sentence is done.");
		}
	}
}
