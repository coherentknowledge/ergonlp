/* File:      SCN2Ergo.java
**
** Author(s): Paul Fodor
**
** Contact:   info@coherentknowledge.com
** 
** Copyright (C) Coherent Knowledge Systems, 2015-2017
** 
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**      http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
** 
*/

package com.coherentknowledge.ergonlp;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;

import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefChain.CorefMention;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;

public class SCN2Ergo {
	private int parseSentenceCount = 0;
	private int count2 = 1;
	private int count3 = 1;
	private Sentence sentenceArray[];
	private Phrast phrast[][];
	private Dependency depArr[][];
	private Coreference corefArr[][];

	private void getTokenPOS(int sentNum, CoreMap sentence) {
		int index = 1;
		for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
			String word = token.get(TextAnnotation.class);
			String pos = token.get(PartOfSpeechAnnotation.class);
			if (index == -1)
				index = 0;
			String phrase_count = "" + ((sentNum * 100) + index);
			String ne = token.get(NamedEntityTagAnnotation.class);
			phrast[sentNum][index] = new Phrast("ph(" + phrase_count + ")", word, pos, index, "s" + sentNum, ne);
			sentenceArray[sentNum].phrastList.add(phrast[sentNum][index]);
			index++;
		}
	}

	private void getDependency(int sentNum, CoreMap sentence) {
		TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
		Tree tree = sentence.get(TreeAnnotation.class);
		tree.pennPrint();
		GrammaticalStructure gs = gsf.newGrammaticalStructure(tree);
		Collection<TypedDependency> tdl = gs.typedDependencies();
		Object[] list = tdl.toArray();
		TypedDependency typedDependency;
		for (Object object : list) {
			typedDependency = (TypedDependency) object;
			int dependencyfrom = typedDependency.gov().index();
			int dependencyto = typedDependency.dep().index();
			String dependencyname = ("dp" + dependencyfrom) + dependencyto;
			depArr[sentNum][count2] = new Dependency(dependencyname,
					(dependencyfrom != 0 ? phrast[sentNum][dependencyfrom] : null), phrast[sentNum][dependencyto],
					typedDependency.reln().toString());
			if (dependencyfrom != 0) {
				if (dependencyfrom > dependencyto)
					phrast[sentNum][dependencyfrom].LeftdpList.add(depArr[sentNum][count2]);
				else
					phrast[sentNum][dependencyfrom].RightdpList.add(depArr[sentNum][count2]);
			}
			if (typedDependency.reln().toString().equalsIgnoreCase("root"))
				sentenceArray[sentNum].root = phrast[sentNum][typedDependency.dep().index()];
			// System.out.println("edge(" + typedDependency.reln().toString()
			// + "," + typedDependency.gov().label().value().toLowerCase()
			// + "," + typedDependency.gov().index() + ","
			// + typedDependency.dep().label().value().toLowerCase() + ","
			// + typedDependency.dep().index() + ").");
			count2++;
		}
	}

	private void getCoref(Annotation document) {
		Map<Integer, CorefChain> graph = document.get(CorefChainAnnotation.class);
		for (Map.Entry entry : graph.entrySet()) {
			CorefChain c = (CorefChain) entry.getValue();
			CorefMention cm = c.getRepresentativeMention();
			List<CorefChain.CorefMention> list = c.getMentionsInTextualOrder();

			if (list.size() == 1) {
				continue;
			} else {
				for (int i = 1; i < list.size(); i++) {

					String refname = ("rf" + list.get(i).headIndex) + list.get(0).headIndex;
					corefArr[list.get(i).sentNum][count3] = new Coreference(refname,
							phrast[list.get(i).sentNum][list.get(i).headIndex],
							phrast[list.get(0).sentNum][list.get(0).headIndex]);
					phrast[list.get(i).sentNum][list.get(i).headIndex].corefList
							.add(corefArr[list.get(i).sentNum][count3]);
					/*
					 * System.out.println("coref(" +
					 * phrast[list.get(i).sentNum][list.get(i).headIndex].
					 * varname + "," +
					 * phrast[list.get(0).sentNum][list.get(0).headIndex].
					 * varname + ").");
					 */
				}
			}
		}
	}

	public void parseSentence(String text) {
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, parse, ner, dcoref"); //
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		Annotation document = new Annotation(text);
		pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		parseSentenceCount = sentences.size() + 1;
		// System.out.println("parseSentenceCount="+parseSentenceCount);
		sentenceArray = new Sentence[sentences.size() + 1];
		phrast = new Phrast[sentences.size() + 1][100];
		depArr = new Dependency[sentences.size() + 1][100];
		corefArr = new Coreference[sentences.size() + 1][100];
		String[] sentenceList = new String[sentences.size() + 1];
		// assign vars
		for (int i = 0; i < sentences.size(); i++) {
			sentenceList[i] = sentences.get(i).toString();
			sentenceArray[i + 1] = new Sentence("" + (i + 1), sentenceList[i]);
			getTokenPOS(i + 1, sentences.get(i));
		}
		// get parser & dependency tree
		for (int i = 0; i < sentences.size(); i++) {
			getDependency(i + 1, sentences.get(i));
		}
		// get coreference
		try {
			getCoref(document);
		} catch (Exception e) {
			System.out.println("Coreference is not working.");
		}
	}

	public void print() {
		for (int i = 1; i < this.parseSentenceCount; i++) {
			// System.out.println(this.sentenceArray[i] + "\n");
			Queue<Phrast> que = new LinkedList<Phrast>();
			que.add(this.sentenceArray[i].root);
			while (!que.isEmpty()) {
				Phrast temp = (Phrast) que.poll();
				for (Dependency d : temp.LeftdpList) {
					que.add(d.dependencyto);
				}
				for (Dependency d : temp.RightdpList) {
					que.add(d.dependencyto);
				}
				// System.out.println(temp + "\n");
			}
		}
	}

	public String print2String() {
		String tString = "";
		for (int i = 1; i < this.parseSentenceCount; i++) {
			tString += "// Sentence: " + this.sentenceArray[i];
			tString += "\n";
			Queue<Phrast> que = new LinkedList<Phrast>();
			que.add(this.sentenceArray[i].root);

			// Determine the type of the sentence
			int sentenceType = 1;
			String sentenceString = this.sentenceArray[i].toString().trim();
			// Print the sentence
			System.out.println(sentenceString);
			char t = sentenceString.charAt(sentenceString.length() - 1);
			if (t == '?')
				sentenceType = 2;
			else if (t == '!')
				sentenceType = 3;

			// Extract the parse
			while (!que.isEmpty()) {
				Phrast temp = (Phrast) que.poll();
				for (Dependency d : temp.LeftdpList) {
					que.add(d.dependencyto);
				}
				for (Dependency d : temp.RightdpList) {
					que.add(d.dependencyto);
				}
				// transform the Phast into Ergo
				if (temp == this.sentenceArray[i].root)
					tString += temp.toString(true, sentenceType);
				else
					tString += temp.toString(false, sentenceType);
				tString += "\n";
			}
		}
		return tString;
	}

	public static void main(String[] args) {
		SCN2Ergo scn2flora = new SCN2Ergo();
		String sentence = "Who is Paul?";
		// System.out.println("% " + sentence);
		scn2flora.parseSentence(sentence);
		scn2flora.print();
		System.out.println("Done");
	}
}
