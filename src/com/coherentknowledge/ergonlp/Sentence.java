/* File:      Sentence.java
**
** Author(s): Paul Fodor
**
** Contact:   info@coherentknowledge.com
** 
** Copyright (C) Coherent Knowledge Systems, 2015-2017
** 
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**      http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
** 
*/

package com.coherentknowledge.ergonlp;

import java.util.ArrayList;

public class Sentence {
	String sentencename;
	ArrayList<Phrast> phrastList;
	Phrast root;
	String sentence;

	public Sentence() {
		super();
	}

	Sentence(String sentencename, String sentence) {
		this.sentencename = sentencename;
		this.sentence = sentence;
		this.phrastList = new ArrayList<Phrast>();
	}

	public String toString() {
		return this.sentence;
	}
}
