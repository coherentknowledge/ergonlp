/* File:      Dependency.java
**
** Author(s): Paul Fodor
**
** Contact:   info@coherentknowledge.com
** 
** Copyright (C) Coherent Knowledge Systems, 2015-2017
** 
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**      http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
** 
*/

package com.coherentknowledge.ergonlp;

public class Dependency {
	String dependencyname;
	Phrast dependencyfrom;
	Phrast dependencyto;
	String dependencytype;

	public Dependency() {
		super();
	}

	Dependency(String dependencyname, Phrast dependencyfrom, Phrast dependencyto, String dependencytype) {
		this.dependencyfrom = dependencyfrom;
		this.dependencyname = dependencyname;
		this.dependencyto = dependencyto;
		this.dependencytype = dependencytype;
	}

	public String toString() {
		String str = "dp(" + this.dependencyname + ").\n" + "dptype(" + this.dependencyname + "," + this.dependencytype
				+ ").\n" + "dpto(" + this.dependencyname + "," + this.dependencyto + ").\n" + "dpfrom("
				+ this.dependencyname + "," + this.dependencyfrom + ").\n";
		return str;
	}
}
