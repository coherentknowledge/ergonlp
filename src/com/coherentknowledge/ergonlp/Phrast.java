/* File:      Phrast.java
**
** Author(s): Paul Fodor
**
** Contact:   info@coherentknowledge.com
** 
** Copyright (C) Coherent Knowledge Systems, 2015-2017
** 
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**      http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
** 
*/

package com.coherentknowledge.ergonlp;

import java.util.ArrayList;

public class Phrast {
	String varname;
	String wordstring;
	String POStag;
	Integer position;
	ArrayList<Dependency> LeftdpList;
	ArrayList<Dependency> RightdpList;
	ArrayList<Coreference> corefList;
	String sentencenum;
	String nameEntity;

	Phrast(String varname, String wordstring, String POStag, Integer position, String sentencenum, String nameEntity) {
		this.varname = varname;
		this.wordstring = wordstring;
		this.POStag = POStag;
		this.position = position;
		LeftdpList = new ArrayList<Dependency>();
		RightdpList = new ArrayList<Dependency>();
		corefList = new ArrayList<Coreference>();
		this.sentencenum = sentencenum;
		this.nameEntity = nameEntity;
	}

	// sentenceType is: 1-statement, 2-question, 3-exclamation/command
	public String toString(boolean root, int sentenceType) {
		String phrastname = this.varname + "[";
		String ws_wd = " ws(wd)->'" + this.wordstring + "',\n";
		String ws_pos = "  ws(PoS)->'" + this.POStag + "'";
		String ldp = "";
		if (LeftdpList.size() != 0) {
			ldp += ",\n";
			for (int i = 0; i < LeftdpList.size(); i++) {
				Dependency dp = LeftdpList.get(i);
				String dptype = dp.dependencytype;
				Phrast dpto = dp.dependencyto;
				if (i != LeftdpList.size() - 1) {
					ldp += "  ldp(" + (i + 1) + ")->dp(" + dptype + "," + dpto.varname + "),\n";
				} else {
					ldp += "  ldp(" + (i + 1) + ")->dp(" + dptype + "," + dpto.varname + ")";
				}
			}
		}
		String ws_ne = "";
		if (this.nameEntity != null && !this.nameEntity.equals("O")) {
			ws_ne = ",\n  ws(ne)->" + this.nameEntity;
		}
		String rdp = "";
		if (RightdpList.size() != 0) {
			rdp += ",\n";
			for (int i = 0; i < RightdpList.size(); i++) {
				Dependency dp = RightdpList.get(i);
				String dptype = dp.dependencytype;
				Phrast dpto = dp.dependencyto;
				if (i != RightdpList.size() - 1) {
					rdp += "  rdp(" + (i + 1) + ")->dp(" + dptype + "," + dpto.varname + "),\n";
				} else {
					rdp += "  rdp(" + (i + 1) + ")->dp(" + dptype + "," + dpto.varname + ")";
				}
			}
		}
		String ws_coref = "";
		if (corefList.size() != 0) {
			ws_coref += ",\n";
			for (int i = 0; i < corefList.size(); i++) {
				if (i != corefList.size() - 1) {
					ws_coref += "  ws(coref)->" + corefList.get(i).refto.varname + ",\n";
				} else {
					ws_coref += "  ws(coref)->" + corefList.get(i).refto.varname;
				}
			}
		}
		String rootString = "";
		if (root) {
			rootString = "  root -> \\true, \n";
			if (sentenceType == 2)
				rootString += "  sentenceType -> question, \n";
			else if (sentenceType == 3)
				rootString += "  sentenceType -> exclamation, \n";
		}
		phrastname = phrastname + ws_wd + rootString + ws_pos + ws_ne + ldp + rdp + ws_coref + "].\n";
		return phrastname;
	}
}
