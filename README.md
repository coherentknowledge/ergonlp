# README #

ErgoNLP is a standalone Java application that interprets natural language (NL) sentences into parse trees that are represented in the Ergo logical language.  Ergo is a language for Rulelog knowledge representation and reasoning (KRR).  ErgoNLP works with the Ergo Lite subset of Ergo as well as with full Ergo.  Info about Ergo is available from its maker Coherent Knowledge, at http://coherentknowledge.com/ .

### Who do I talk to? And how do I find out more? ###

* You can contact Coherent Knowledge at info@coherentknowledge.com .  There is lots of info overall, at its website http://coherentknowledge.com/ .

### Copyright info ###

* Copyright (C) Coherent Knowledge Systems, 2015-2017
 
### License info ###  

* ErgoNLP is licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 .

* Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* ErgoNLP uses the Stanford CoreNLP libraries licensed under the GNU General Public License (GPL) v3 available here: https://www.gnu.org/licenses/gpl.html .